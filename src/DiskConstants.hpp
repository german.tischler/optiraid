/*
    optiraid
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(DISKCONTANTS_HPP)
#define DISKCONTANTS_HPP

#include <libmaus2/types/types.hpp>
#include <cassert>

struct DiskConstants
{
	uint64_t const disksize;
	uint64_t const blocks;
	uint64_t const disks;
	uint64_t const diskmetasize;
	uint64_t const digestlength;
	uint64_t const inputsize;
	uint64_t const rawstripesize;
	uint64_t const rawblocksize;
	uint64_t const blockheadersize;
	uint64_t const blockpayloadsize;
	uint64_t const payloadperbatch;
	uint64_t const numbatches;
	uint64_t const stripesize;
	uint64_t const substripesize;

	DiskConstants(
		uint64_t const rdisksize,
		uint64_t const rblocks,
		uint64_t const rdisks,
		uint64_t const rdiskmetasize,
		uint64_t const rdigestlength,
		uint64_t const rinputsize,
		uint64_t const rblockheadersize
	) : disksize(rdisksize), blocks(rblocks), disks(rdisks), diskmetasize(rdiskmetasize),
	    digestlength(rdigestlength),
	    inputsize(rinputsize),
	    rawstripesize((disksize - 2*diskmetasize) / disks),
	    rawblocksize(rawstripesize / (blocks+1)),
	    blockheadersize(rblockheadersize), // sizeof(uint64_t) + sizeof(uint64_t) + digestlength),
	    blockpayloadsize(rawblocksize - blockheadersize),
	    payloadperbatch(blockpayloadsize * blocks * disks /* stripes per disk */ * (disks-1)),
	    numbatches( (inputsize + payloadperbatch - 1)/payloadperbatch),
	    stripesize(blockpayloadsize * blocks * (disks-1)),
	    substripesize(blockpayloadsize * blocks)
	{
		assert ( rawblocksize >= blockheadersize );
	}
};
#endif
