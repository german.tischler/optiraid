/*
    optiraid
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(BLOCKHEADER_HPP)
#define BLOCKHEADER_HPP

#include <libmaus2/util/NumberSerialisation.hpp>
#include <libmaus2/autoarray/AutoArray.hpp>
#include <libmaus2/digest/DigestInterface.hpp>

struct BlockHeader
{
	uint64_t stripeid;
	uint64_t blockid;
	libmaus2::autoarray::AutoArray<uint8_t> Adigest;

	BlockHeader()
	{}

	BlockHeader(
		uint64_t const rstripeid,
		uint64_t const rblockid,
		libmaus2::autoarray::AutoArray<uint8_t> const & rAdigest
	) : stripeid(rstripeid), blockid(rblockid), Adigest(rAdigest.clone()) {}

	static uint64_t getSize(libmaus2::digest::DigestInterface & datadigest, libmaus2::digest::DigestInterface & headerdigest)
	{
		return 2 * sizeof(uint64_t) + datadigest.vdigestlength() + headerdigest.vdigestlength();
	}

	void serialiseData(std::ostream & out, libmaus2::digest::DigestInterface & datadigest) const
	{
		libmaus2::util::NumberSerialisation::serialiseNumber(out,stripeid);
		libmaus2::util::NumberSerialisation::serialiseNumber(out,blockid);
		out.write(reinterpret_cast<char const *>(Adigest.begin()),datadigest.vdigestlength());

		if ( ! out )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] BlockHeader::serialiseData: failed to write data digest" << std::endl;
			lme.finish();
			throw lme;
		}
	}

	void serialise(std::ostream & out, libmaus2::digest::DigestInterface & datadigest, libmaus2::digest::DigestInterface & headerdigest) const
	{
		std::ostringstream datastr;
		serialiseData(datastr,datadigest);
		std::string const data = datastr.str();
		libmaus2::autoarray::AutoArray<uint8_t> Aheaderdigest(headerdigest.vdigestlength());
		headerdigest.vinit();
		headerdigest.vupdate(reinterpret_cast<uint8_t const *>(data.c_str()),data.size());
		headerdigest.digest(Aheaderdigest.begin());

		out.write(data.c_str(),data.size());
		if ( ! out )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] BlockHeader::serialise: failed to write header data" << std::endl;
			lme.finish();
			throw lme;
		}

		out.write(reinterpret_cast<char const *>(Aheaderdigest.begin()),headerdigest.vdigestlength());
		if ( ! out )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] BlockHeader::serialise: failed to write header digest" << std::endl;
			lme.finish();
			throw lme;
		}
	}

	void deserialiseData(std::istream & in, libmaus2::digest::DigestInterface & datadigest)
	{
		stripeid = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		blockid = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		Adigest.resize(datadigest.vdigestlength());

		in.read(
			reinterpret_cast<char *>(Adigest.begin()),
			datadigest.vdigestlength()
		);

		if ( ! in || in.gcount() != static_cast<std::streamsize>(datadigest.vdigestlength()) )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] BlockHeader::deserialiseData: failed to read data digest" << std::endl;
			lme.finish();
			throw lme;
		}
	}

	void deserialise(std::istream & in, libmaus2::digest::DigestInterface & datadigest, libmaus2::digest::DigestInterface & headerdigest)
	{
		deserialiseData(in,datadigest);
		std::ostringstream datastr;
		serialiseData(datastr,datadigest);
		std::string const data = datastr.str();

		libmaus2::autoarray::AutoArray<uint8_t> Aheaderdigest(headerdigest.vdigestlength());
		headerdigest.vinit();
		headerdigest.vupdate(reinterpret_cast<uint8_t const *>(data.c_str()),data.size());
		headerdigest.digest(Aheaderdigest.begin());

		libmaus2::autoarray::AutoArray<uint8_t> Aheaderdigestdisk(headerdigest.vdigestlength());
		in.read(reinterpret_cast<char *>(Aheaderdigestdisk.begin()),headerdigest.vdigestlength());
		if ( ! in || in.gcount() != static_cast<std::streamsize>(headerdigest.vdigestlength()) )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] BlockHeader::deserialise: failed to read header digest" << std::endl;
			lme.finish();
			throw lme;
		}

		if ( memcmp(Aheaderdigest.begin(),Aheaderdigestdisk.begin(),headerdigest.vdigestlength()) != 0 )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] BlockHeader::deserialise: header digest mismatch" << std::endl;
			lme.finish();
			throw lme;
		}

		// std::cerr << "[V] block header digest " << headerdigest.vdigestToString(Aheaderdigest.begin()) << std::endl;
	}
};
#endif
