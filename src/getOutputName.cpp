/*
    optiraid
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <getOutputName.hpp>

#include <sstream>
#include <iomanip>

std::string getOutputName(std::string const prefix, uint64_t const i, uint64_t const j)
{
	std::ostringstream ostr;

	ostr << prefix
		<< "_" << std::setw(6) << std::setfill('0') << i << std::setw(0)
		<< "_" << std::setw(6) << std::setfill('0') << j << std::setw(0)
		;

	return ostr.str();
}
