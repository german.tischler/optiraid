/*
    optiraid
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(DISKHEADERELEMENT_HPP)
#define DISKHEADERELEMENT_HPP

#include <libmaus2/util/StringSerialisation.hpp>

struct DiskHeaderElement
{
	std::string key;
	std::string value;

	static std::string getMagic()
	{
		return "DISKHEADELEM";
	}

	static uint64_t getMaxLength()
	{
		std::ostringstream ostr;
		ostr << std::numeric_limits<uint64_t>::max();
		return ostr.str().size();
	}

	DiskHeaderElement() {}
	DiskHeaderElement(std::string const & rkey, std::string const & rvalue) : key(rkey), value(rvalue) {}
	DiskHeaderElement(std::string const & rkey, uint64_t const uvalue) : key(rkey) {
		std::ostringstream ostr;
		ostr << std::setw(getMaxLength()) << uvalue << std::setw(0);
		value = ostr.str();
	}

	uint64_t getValueAsU64() const
	{
		std::istringstream istr(value);
		uint64_t u;
		istr >> u;

		if ( ! istr || istr.peek() != std::istream::traits_type::eof() )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] DiskHeaderElement::getValueAsU64: cannot parse value " << value << " for key " << key << " as unsigned integer" << std::endl;
			lme.finish();
			throw lme;
		}

		return u;
	}

	template<typename output_stream>
	uint64_t serialise(output_stream & out) const
	{
		uint64_t c = 0;

		c += libmaus2::util::StringSerialisation::serialiseString(out,getMagic());
		c += libmaus2::util::StringSerialisation::serialiseString(out,key);
		c += libmaus2::util::StringSerialisation::serialiseString(out,value);

		return c;
	}

	uint64_t deserialise(std::istream & in)
	{
		std::string const magic = libmaus2::util::StringSerialisation::deserialiseString(in);

		if ( magic != getMagic() )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] DiskHeaderElement::deserialise: magic mismatch" << std::endl;
			lme.finish();
			throw lme;
		}

		key = libmaus2::util::StringSerialisation::deserialiseString(in);
		value = libmaus2::util::StringSerialisation::deserialiseString(in);

		std::ostringstream ostr;
		serialise(ostr);

		return ostr.str().size();
	}
};
#endif
