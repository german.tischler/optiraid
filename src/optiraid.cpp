/*
    optiraid
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <DiskHeader.hpp>
#include <DiskConstants.hpp>
#include <getOutputName.hpp>
#include <BlockHeader.hpp>
#include <getHeaderDigestName.hpp>

#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/util/StringSerialisation.hpp>
#include <libmaus2/util/TempFileRemovalContainer.hpp>
#include <libmaus2/digest/DigestFactory.hpp>
#include <libmaus2/util/GetFileSize.hpp>

static uint64_t getDefaultDiskSize()
{
	return 25ull*1000ull*1000ull*1000ull;
}

static void readBlock(std::istream & in, uint64_t a, uint64_t b, uint64_t l, char * const B)
{
	assert ( b >= a );

	memset(B,0,b-a);

	a = std::min(a,l);
	b = std::min(b,l);

	if ( b > a )
	{
		in.clear();
		in.seekg(a);
		in.read(B,b-a);
		if ( ! in || in.gcount() != static_cast<std::streamsize>(b-a) )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "[E] failed to read block of size " << b-a << " at position " << a << std::endl;
			lme.finish();
			throw lme;
		}
	}
}

uint64_t previousMultiple(uint64_t const disksize, uint64_t const diskblocksize)
{
	return (disksize / diskblocksize) * diskblocksize;
}

int optiraid(libmaus2::util::ArgParser const & arg)
{
	uint64_t const verbose = arg.uniqueArgPresent("v") ? arg.getUnsignedNumericArg<uint64_t>("v") : 1;

	std::string const tmpfilebase = arg.uniqueArgPresent("T") ? arg["T"] : libmaus2::util::ArgInfo::getDefaultTmpFileName(arg.progname);
	std::string const tmpfn = tmpfilebase + "_blocks";
	libmaus2::util::TempFileRemovalContainer::addTempFile(tmpfn);
	uint64_t const readblocksize = arg.uniqueArgPresent("B") ? arg.getUnsignedNumericArg<uint64_t>("B") : 32*1024ull*1024ull;
	libmaus2::autoarray::AutoArray<char> B(readblocksize,false);
	libmaus2::autoarray::AutoArray<char> C(readblocksize,false);

	std::string const output = arg[0];
	std::string const input = arg[1];

	libmaus2::aio::InputStreamInstance ISI(input);

	uint64_t const inputsize = libmaus2::util::GetFileSize::getFileSize(ISI);

	if ( verbose )
		std::cerr << "[V] input " << input << " of size " << inputsize << std::endl;

	std::set<std::string> const Sdigsup = libmaus2::digest::DigestFactory::getSupportedDigestsStatic();

	std::string sdig = "sha256";

	if ( Sdigsup.find(sdig) == Sdigsup.end() )
	{
		std::cerr << "[E] digest " << sdig << " is not supported in libmaus2" << std::endl;
		return EXIT_FAILURE;
	}

	libmaus2::digest::DigestInterface::unique_ptr_type uheaderdigest(libmaus2::digest::DigestFactory::constructStatic(getHeaderDigestName()));
	libmaus2::digest::DigestInterface::unique_ptr_type udatadigest(libmaus2::digest::DigestFactory::constructStatic(sdig));
	uint64_t const datadigestlength = udatadigest->vdigestlength();
	libmaus2::autoarray::AutoArray<uint8_t> Adigest(datadigestlength,false);

	uint64_t inputblocks = (inputsize + readblocksize - 1)/readblocksize;

	ISI.clear();
	ISI.seekg(0);

	udatadigest->vinit();
	for ( uint64_t iblockid = 0; iblockid < inputblocks; ++iblockid )
	{
		uint64_t const low = iblockid * readblocksize;
		uint64_t const high = std::min(low + readblocksize,inputsize);
		assert ( high > low );
		uint64_t const r = high - low;

		ISI.read(B.begin(),r);
		if ( ! ISI || ISI.gcount() != static_cast<std::streamsize>(r) )
		{
			std::cerr << "[E] failed to read input file for computing digest" << std::endl;
			return EXIT_FAILURE;
		}

		udatadigest->vupdate(reinterpret_cast<uint8_t const *>(B.begin()),r);
		
		std::cerr << "[V] digest computation " << (iblockid+1) << "/" << inputblocks << std::endl;
	}
	udatadigest->digest(Adigest.begin());
	std::string const inputdigest = udatadigest->vdigestToString(Adigest.begin());

	if ( verbose )
		std::cerr << "[V] input digest " << inputdigest << std::endl;

	ISI.clear();
	ISI.seekg(0);


	if ( verbose )
		std::cerr << "[V] block digest length " << datadigestlength << std::endl;

	if ( ! arg.uniqueArgPresent("d") )
	{
		std::cerr << "[E] mandatory number of disk arg (-d) missing" << std::endl;
		return EXIT_FAILURE;
	}


	if ( ! arg.uniqueArgPresent("b") )
	{
		std::cerr << "[E] mandatory number of disk arg (-b) missing" << std::endl;
		return EXIT_FAILURE;
	}

	uint64_t const blocks = arg.getUnsignedNumericArg<uint64_t>("b");
	uint64_t const diskblocksize = arg.uniqueArgPresent("B") ? arg.getUnsignedNumericArg<uint64_t>("B") : (32*1024);
	uint64_t const disksize = arg.uniqueArgPresent("disksize") ?
		previousMultiple(arg.getUnsignedNumericArg<uint64_t>("disksize"),diskblocksize)
		:
		previousMultiple(getDefaultDiskSize(),diskblocksize);
	uint64_t const disks = arg.getUnsignedNumericArg<uint64_t>("d");

	if ( disks < 2 )
	{
		std::cerr << "[E] value for -d parameter needs to be > 1" << std::endl;
		return EXIT_FAILURE;
	}

	uint64_t const diskheadersize = DiskHeader(disksize,disks,blocks,inputsize,0 /* batch */,0 /* diskid */,sdig,inputdigest).getSize(*uheaderdigest);
	assert ( diskheadersize <= disksize );

	if ( verbose )
		std::cerr << "[V] diskheadersize " << diskheadersize << std::endl;

	uint64_t const blockheadersize = BlockHeader::getSize(*udatadigest,*uheaderdigest);

	DiskConstants DC(disksize,blocks,disks,diskheadersize,datadigestlength,inputsize,blockheadersize);

	/*

	 Format per disk:

	 DISKMAGIC | DISKHEADERLEN(8BE) | DISKHEADER

	 BLOCK*

	 Block format:

	 STRIPE ID (8BE)
	 BLOCK ID (8BE)
	 DIGEST
	 DATA

	 */

	uint64_t const readblockspersubstripe = (DC.substripesize + readblocksize - 1)/readblocksize;
	uint64_t const readblocksperblock = (DC.blockpayloadsize + readblocksize - 1)/readblocksize;

	uint64_t const expectedblocks = disks * (blocks+1);
	uint64_t const expectedposition =
			diskheadersize
			+
			expectedblocks * (
				blockheadersize
				+
				DC.blockpayloadsize
			);
	uint64_t const padding = disksize - (expectedposition + diskheadersize);

	if ( verbose )
	{
		std::cerr << "[V] disk size:               " << disksize << std::endl;
		std::cerr << "[V] payload bytes per batch: " << DC.payloadperbatch  << std::endl;
		std::cerr << "[V] number of batches:       " << DC.numbatches << std::endl;
		std::cerr << "[V] padding per disk:        " << padding << std::endl;
	}

	for ( uint64_t batch = 0; batch < DC.numbatches; ++batch )
	{
		// payload window
		uint64_t const batchlow  = batch * DC.payloadperbatch;
		uint64_t const batchhigh = (batch+1) * DC.payloadperbatch;

		if ( verbose )
			std::cerr << "batch " << batch << " [" << batchlow << "," << batchhigh << ")" << std::endl;

		std::vector< std::pair<uint64_t,uint64_t> > VI;

		for ( uint64_t diskid = 0; diskid < disks; ++diskid )
		{
			if ( verbose )
				std::cerr << "\tdisk " << diskid << std::endl;

			libmaus2::aio::OutputStreamInstance::unique_ptr_type pBOSI(
				new libmaus2::aio::OutputStreamInstance(tmpfn)
			);

			for ( uint64_t stripeid = 0; stripeid < disks; ++stripeid )
			{
				uint64_t const stripelow  = batchlow + DC.blockpayloadsize * (stripeid+0) * blocks * (disks-1);
				uint64_t const stripehigh = batchlow + DC.blockpayloadsize * (stripeid+1) * blocks * (disks-1);
				assert ( stripehigh - stripelow == DC.stripesize );

				uint64_t const sub = (stripeid + diskid) % (disks);

				if ( sub+1 < disks )
				{
					uint64_t const sublow  = stripelow + DC.blockpayloadsize * blocks * (sub+0);
					uint64_t const subhigh = stripelow + DC.blockpayloadsize * blocks * (sub+1);

					if ( verbose )
						std::cerr << "\t\tstripe " << stripeid << "=[" << stripelow << "," << stripehigh << ")" << " sub " << sub << "=[" << sublow << "," << subhigh << ")" << std::endl;

					assert ( subhigh - sublow == DC.substripesize );

					uint64_t offsum = 0;
					for ( uint64_t readblockid = 0; readblockid < readblockspersubstripe; ++readblockid )
					{
						uint64_t const readblocklow  = sublow + readblockid * readblocksize;
						uint64_t const readblockhigh = std::min(readblocklow+readblocksize,subhigh);
						assert ( readblockhigh-readblocklow <= readblocksize );
						offsum += readblockhigh - readblocklow;

						readBlock(ISI, readblocklow, readblockhigh, inputsize, B.begin());

						pBOSI->write(B.begin(),readblockhigh-readblocklow);

						VI.push_back(std::pair<uint64_t,uint64_t>(readblocklow,readblockhigh));
					}

					assert ( offsum == subhigh-sublow );
				}
				else
				{
					if ( verbose )
						std::cerr << "\t\tstripe " << stripeid << "=[" << stripelow << "," << stripehigh << ")" << " sub " << "par" << std::endl;

					#if defined(DEBUG)
					std::vector<std::pair<uint64_t,uint64_t> > LVI;
					#endif

					uint64_t offsum = 0;
					for ( uint64_t readblockid = 0; readblockid < readblockspersubstripe; ++readblockid )
					{
						uint64_t const offlow = readblockid * readblocksize;
						uint64_t const offhigh = std::min(offlow + readblocksize,DC.substripesize);
						uint64_t const offd = offhigh - offlow;
						offsum += offd;
						assert ( offd <= readblocksize );

						memset(C.begin(),0,offd);

						for ( uint64_t subid = 0; subid+1 < disks; ++subid )
						{
							uint64_t const sublow = stripelow + DC.blockpayloadsize * blocks * (subid+0);
							uint64_t const subhigh = stripelow + DC.blockpayloadsize * blocks * (subid+1);
							uint64_t const readblocklow = sublow + readblockid * readblocksize;
							uint64_t const readblockhigh = std::min(readblocklow+readblocksize,subhigh);
							assert ( readblockhigh-readblocklow == offd );

							readBlock(ISI, readblocklow, readblockhigh, inputsize, B.begin());

							uint8_t       * uc = reinterpret_cast<uint8_t *>(C.begin());
							uint8_t const * ub = reinterpret_cast<uint8_t *>(B.begin());
							uint8_t const * ube = ub + offd;

							while ( ub != ube )
								*uc++ ^= *ub++;

							#if defined(DEBUG)
							std::cerr << "\t\t[" << readblocklow << "," << readblockhigh << ")" << std::endl;

							LVI.push_back(std::pair<uint64_t,uint64_t>(readblocklow,readblockhigh));
							#endif
						}

						pBOSI->write(C.begin(),offd);
					}

					assert ( offsum == DC.substripesize );

					#if defined(DEBUG)
					std::sort(LVI.begin(),LVI.end());
					assert ( LVI[0].first == stripelow );
					assert ( LVI.back().second == stripehigh );
					#endif
				}
			}

			pBOSI->flush();
			pBOSI.reset();

			uint64_t const blockfilesize = libmaus2::util::GetFileSize::getFileSize(tmpfn);
			assert ( blockfilesize == disks * DC.substripesize );

			std::string const diskfn = getOutputName(output,batch,diskid);
			// open disk file
			libmaus2::aio::OutputStreamInstance::unique_ptr_type pDOSI(
				new libmaus2::aio::OutputStreamInstance(diskfn)
			);

			DiskHeader const DHout(disksize,disks,blocks,inputsize,batch,diskid,sdig,inputdigest);
			DHout.serialise(*pDOSI,*uheaderdigest);

			assert ( pDOSI->tellp() == static_cast<std::streamsize>(diskheadersize) );

			libmaus2::aio::InputStreamInstance::unique_ptr_type pBISI(
				new libmaus2::aio::InputStreamInstance(tmpfn)
			);

			uint64_t offsum = 0;
			for ( uint64_t blockid = 0; blockid < blocks; ++blockid )
			{
				if ( verbose )
					std::cerr << "\t\tblockid=" << blockid << std::endl;

				for ( uint64_t stripeid = 0; stripeid < disks; ++stripeid )
				{
					uint64_t const offlow = stripeid * DC.substripesize + blockid * DC.blockpayloadsize;
					uint64_t const offhigh = offlow + DC.blockpayloadsize;
					uint64_t const offd = offhigh-offlow;
					offsum += offd;

					pBISI->clear();
					pBISI->seekg(offlow);

					// stripeid,blockid

					udatadigest->vinit();

					uint64_t subsum = 0;
					for ( uint64_t subid = 0; subid < readblocksperblock; ++subid )
					{
						uint64_t const sublow = offlow + subid*readblocksize;
						uint64_t const subhigh = std::min(sublow+readblocksize,offhigh);
						uint64_t const subd = subhigh-sublow;
						subsum += subd;

						pBISI->read(B.begin(),subd);
						assert ( *pBISI && pBISI->gcount() == static_cast<std::streamsize>(subd) );

						udatadigest->vupdate(
							reinterpret_cast<uint8_t const *>(B.begin()),
							subd
						);
					}

					udatadigest->digest(Adigest.begin());

					std::string const sdigest(udatadigest->vdigestToString(Adigest.begin()));

					if ( verbose )
						std::cerr << "\t\t\tstripeid=" << stripeid << " " << sdigest << std::endl;

					assert ( subsum == offd );

					// write block header
					BlockHeader(stripeid,blockid,Adigest).serialise(*pDOSI,*udatadigest,*uheaderdigest);

					#if 0
					libmaus2::util::NumberSerialisation::serialiseNumber(*pDOSI,stripeid);
					libmaus2::util::NumberSerialisation::serialiseNumber(*pDOSI,blockid);
					pDOSI->write(reinterpret_cast<char const *>(Adigest.begin()),datadigestlength);
					if ( ! *pDOSI )
					{
						libmaus2::exception::LibMausException lme;
						lme.getStream() << "[E] failed to write digest to data file" << std::endl;
						lme.finish();
						throw lme;
					}
					#endif

					pBISI->clear();
					pBISI->seekg(offlow);
					subsum = 0;

					for ( uint64_t subid = 0; subid < readblocksperblock; ++subid )
					{
						uint64_t const sublow = offlow + subid*readblocksize;
						uint64_t const subhigh = std::min(sublow+readblocksize,offhigh);
						uint64_t const subd = subhigh-sublow;
						subsum += subd;

						pBISI->read(B.begin(),subd);
						assert ( *pBISI && pBISI->gcount() == static_cast<std::streamsize>(subd) );

						pDOSI->write(B.begin(),subd);
						if ( ! *pDOSI )
						{
							libmaus2::exception::LibMausException lme;
							lme.getStream() << "[E] failed to write data to data file" << std::endl;
							lme.finish();
							throw lme;
						}

						#if 0
						udatadigest->vupdate(
							reinterpret_cast<uint8_t const *>(B.begin()),
							subd
						);
						#endif
					}

					assert ( subsum == offd );
				}
			}
			assert ( offsum == blockfilesize );

			if ( verbose )
				std::cerr << "\t\tblockid=par" << std::endl;

			for ( uint64_t stripeid = 0; stripeid < disks; ++stripeid )
			{
				uint64_t const stripelow  = stripeid * DC.substripesize; // + blockid * DC.blockpayloadsize;
				uint64_t const stripehigh = stripelow + DC.substripesize;
				uint64_t const striped    = stripehigh-stripelow;
				uint64_t stripesum = 0;

				udatadigest->vinit();

				for ( uint64_t readblockid = 0; readblockid < readblocksperblock; ++readblockid )
				{
					uint64_t const blocklow  = readblockid * readblocksize;
					uint64_t const blockhigh = std::min(blocklow+readblocksize,DC.blockpayloadsize);
					uint64_t const blockd = blockhigh-blocklow;

					memset(C.begin(),0,blockd);

					for ( uint64_t blockid = 0; blockid < blocks; ++blockid )
					{
						uint64_t const offlow  = stripelow + blockid * DC.blockpayloadsize + blocklow;
						uint64_t const offhigh = stripelow + blockid * DC.blockpayloadsize + blockhigh;
						uint64_t const offd = offhigh-offlow;

						pBISI->clear();
						pBISI->seekg(offlow);
						pBISI->read(B.begin(),offd);
						assert ( *pBISI && pBISI->gcount() == static_cast<std::streamsize>(offd) );

						uint8_t const * ub = reinterpret_cast<uint8_t const *>(B.begin());
						uint8_t const * ube = ub + offd;
						uint8_t * uc = reinterpret_cast<uint8_t *>(C.begin());

						while ( ub != ube )
							*uc++ ^= *ub++;

						stripesum += offd;
					}

					udatadigest->vupdate(reinterpret_cast<uint8_t const *>(C.begin()),blockd);
				}

				udatadigest->digest(Adigest.begin());

				std::string const sdigest(udatadigest->vdigestToString(Adigest.begin()));

				if ( verbose )
					std::cerr << "\t\t\tstripeid=" << stripeid << " " << sdigest << std::endl;

				// write block header
				BlockHeader(stripeid,blocks /* par */,Adigest).serialise(*pDOSI,*udatadigest,*uheaderdigest);

				#if 0
				libmaus2::util::NumberSerialisation::serialiseNumber(*pDOSI,stripeid);
				libmaus2::util::NumberSerialisation::serialiseNumber(*pDOSI,blocks /* par */);
				pDOSI->write(reinterpret_cast<char const *>(Adigest.begin()),datadigestlength);
				if ( ! *pDOSI )
				{
					libmaus2::exception::LibMausException lme;
					lme.getStream() << "[E] failed to write digest to data file" << std::endl;
					lme.finish();
					throw lme;
				}
				#endif

				assert ( stripesum == DC.substripesize );
				assert ( stripesum == striped );

				stripesum = 0;

				for ( uint64_t readblockid = 0; readblockid < readblocksperblock; ++readblockid )
				{
					uint64_t const blocklow  = readblockid * readblocksize;
					uint64_t const blockhigh = std::min(blocklow+readblocksize,DC.blockpayloadsize);
					uint64_t const blockd = blockhigh-blocklow;

					memset(C.begin(),0,blockd);

					for ( uint64_t blockid = 0; blockid < blocks; ++blockid )
					{
						uint64_t const offlow  = stripelow + blockid * DC.blockpayloadsize + blocklow;
						uint64_t const offhigh = stripelow + blockid * DC.blockpayloadsize + blockhigh;
						uint64_t const offd = offhigh-offlow;

						pBISI->clear();
						pBISI->seekg(offlow);
						pBISI->read(B.begin(),offd);
						assert ( *pBISI && pBISI->gcount() == static_cast<std::streamsize>(offd) );

						uint8_t const * ub = reinterpret_cast<uint8_t const *>(B.begin());
						uint8_t const * ube = ub + offd;
						uint8_t * uc = reinterpret_cast<uint8_t *>(C.begin());

						while ( ub != ube )
							*uc++ ^= *ub++;

						stripesum += offd;
					}

					pDOSI->write(C.begin(),blockd);
					if ( ! *pDOSI )
					{
						libmaus2::exception::LibMausException lme;
						lme.getStream() << "[E] failed to write data to data file" << std::endl;
						lme.finish();
						throw lme;
					}

				}

				assert ( stripesum == DC.substripesize );
				assert ( stripesum == striped );
			}

			{

				assert ( pDOSI->tellp() == static_cast<std::streamsize>(expectedposition) );

				assert ( disksize >= expectedposition + diskheadersize );

				uint64_t const paddingblocksize = 64*1024;
				uint64_t paddingrem = padding;

				libmaus2::autoarray::AutoArray<char> A(paddingblocksize,false);
				std::fill(A.begin(),A.end(),0);

				while ( paddingrem )
				{
					uint64_t const towrite = std::min(paddingrem,paddingblocksize);

					pDOSI->write(A.begin(),towrite);

					if ( ! *pDOSI )
					{
						libmaus2::exception::LibMausException lme;
						lme.getStream() << "[E] failed to write padding data to data file" << std::endl;
						lme.finish();
						throw lme;
					}

					paddingrem -= towrite;
				}

				std::cerr << "\t\twrote " << padding << " padding bytes" << std::endl;
			}

			DHout.serialise(*pDOSI,*uheaderdigest);

			assert ( pDOSI->tellp() == static_cast<std::streamsize>(disksize) );

			pDOSI->flush();
			pDOSI.reset();

			{
				libmaus2::aio::InputStreamInstance backISI(diskfn);
				DiskHeader DH;
				DH.deserialise(backISI,*uheaderdigest);

				// #define DATADEBUG

				#if defined(DATADEBUG)
				struct DataElement
				{
					uint64_t stripeid;
					uint64_t blockid;
					std::string data;

					DataElement() {}
					DataElement(uint64_t const rstripeid, uint64_t const rblockid, std::string const & rdata)
					: stripeid(rstripeid), blockid(rblockid), data(rdata) {}

					bool operator<(DataElement const & DE) const
					{
						if ( stripeid != DE.stripeid )
							return stripeid < DE.stripeid;
						else
							return blockid < DE.blockid;
					}

					std::string xorop(std::string const & odata) const
					{
						uint8_t const * ua = reinterpret_cast<uint8_t const *>(data.c_str());
						uint8_t const * ub = reinterpret_cast<uint8_t const *>(odata.c_str());
						std::vector<uint8_t> V;

						for ( uint64_t i = 0; i < data.size(); ++i )
							V.push_back(ua[i] ^ ub[i]);

						return std::string(V.begin(),V.end());
					}
				};

				std::vector<DataElement> VDE;
				#endif

				uint64_t const expectedblocks = DH.disks * (DH.blocks+1);

				for ( uint64_t blocknum = 0; blocknum < expectedblocks; ++blocknum )
				{
					BlockHeader BH;
					BH.deserialise(backISI,*udatadigest,*uheaderdigest);

					uint64_t const stripeid = BH.stripeid;
					uint64_t const blockid = BH.blockid;

					#if 0
					// read block header
					uint64_t const stripeid = libmaus2::util::NumberSerialisation::deserialiseNumber(backISI);
					uint64_t const blockid = libmaus2::util::NumberSerialisation::deserialiseNumber(backISI);
					libmaus2::autoarray::AutoArray<uint8_t> Adigest(datadigestlength);
					backISI.read(reinterpret_cast<char *>(Adigest.begin()),datadigestlength);

					if ( ! backISI )
					{
						libmaus2::exception::LibMausException lme;
						lme.getStream() << "[E] failed to read digest" << std::endl;
						lme.finish();
						throw lme;
					}
					#endif

					// uint64_t const sub = (stripeid + diskid) % (disks);
					std::string const sdigest_expected(udatadigest->vdigestToString(BH.Adigest.begin()));

					udatadigest->vinit();

					#if defined(DATADEBUG)
					std::ostringstream datastr;
					#endif
					for ( uint64_t readblockid = 0; readblockid < readblocksperblock; ++readblockid )
					{
						uint64_t const blocklow  = readblockid * readblocksize;
						uint64_t const blockhigh = std::min(blocklow+readblocksize,DC.blockpayloadsize);
						uint64_t const blockd = blockhigh-blocklow;

						backISI.read(B.begin(),blockd);

						#if defined(DATADEBUG)
						datastr.write(B.begin(),blockd);
						#endif

						udatadigest->vupdate(reinterpret_cast<uint8_t const *>(B.begin()),blockd);
					}

					udatadigest->digest(Adigest.begin());

					std::string const sdigest_ondisk(udatadigest->vdigestToString(Adigest.begin()));

					if ( verbose > 1 )
						std::cerr << "\t\tstripeid=" << stripeid << " blockid=" << blockid << " " << sdigest_expected << " " << sdigest_ondisk << std::endl;

					assert ( sdigest_expected == sdigest_ondisk );

					#if defined(DATADEBUG)
					std::string const data = datastr.str();
					VDE.push_back(DataElement(stripeid,blockid,data));
					#endif
				}

				backISI.ignore(padding);

				DiskHeader DHend;
				DHend.deserialise(backISI,*uheaderdigest);

				// std::cerr << "[V] disk header at end " << DHend << std::endl;

				#if defined(DATADEBUG)
				std::sort(VDE.begin(),VDE.end());

				uint64_t ilow = 0;
				while ( ilow < VDE.size() )
				{
					uint64_t ihigh = ilow+1;

					while ( ihigh < VDE.size() && VDE[ihigh].stripeid == VDE[ilow].stripeid )
						++ihigh;

					for ( uint64_t i = 0; i < blocks; ++i )
						assert ( VDE[ilow+i].blockid == i );

					std::string data = VDE[ilow].data;
					for ( uint64_t i = 1; i < blocks; ++i )
						data = VDE[ilow+i].xorop(data);

					assert ( data == VDE[ilow+blocks].data );

					uint64_t const stripeid = VDE[ilow].stripeid;

					uint64_t const stripelow  = batchlow + DC.blockpayloadsize * (stripeid+0) * blocks * (disks-1);
					uint64_t const stripehigh = batchlow + DC.blockpayloadsize * (stripeid+1) * blocks * (disks-1);
					assert ( stripehigh - stripelow == DC.stripesize );
					uint64_t const sub = (stripeid + diskid) % (disks);

					if ( sub+1 < disks )
					{
						uint64_t const sublow  = stripelow + DC.blockpayloadsize * blocks * (sub+0);
						// uint64_t const subhigh = stripelow + DC.blockpayloadsize * blocks * (sub+1);

						for ( uint64_t i = 0; i < blocks; ++i )
						{
							uint64_t const blocklow = sublow + i * DC.blockpayloadsize;
							uint64_t const blockhigh = blocklow + DC.blockpayloadsize;

							uint64_t const checklow = std::min(blocklow,inputsize);
							uint64_t const checkhigh = std::min(blockhigh,inputsize);

							if ( checkhigh > checklow )
							{
								libmaus2::autoarray::AutoArray<char> BB(checkhigh-checklow,false);
								ISI.clear();
								ISI.seekg(blocklow);
								ISI.read(BB.begin(),checkhigh-checklow);
								assert ( ISI && ISI.gcount() == static_cast<std::streamsize>(checkhigh-checklow) );

								assert (
									std::string(
										BB.begin(),
										BB.begin() + (checkhigh-checklow)
									)
									==
									VDE[ilow+i].data.substr(0,checkhigh-checklow)
								);

								if ( verbose )
									std::cerr << "checked " << (checkhigh-checklow) << std::endl;
							}
						}
					}

					if ( verbose )
						std::cerr << "checked stripeid " << VDE[ilow].stripeid << std::endl;

					ilow = ihigh;
				}
				#endif

				// std::cerr << DH << std::endl;
			}


			// std::cerr << "tmp file size " << libmaus2::util::GetFileSize::getFileSize(tmpfn) << std::endl;

			libmaus2::aio::FileRemoval::removeFile(tmpfn);
		}

		std::sort(VI.begin(),VI.end());
		for ( uint64_t i = 1; i < VI.size(); ++i )
			assert ( VI[i-1].second == VI[i].first );
		assert ( VI.size() );
		assert ( VI[0].first == batchlow );
		assert ( VI.back().second == batchhigh );
	}

	return EXIT_SUCCESS;
}

static void usage(libmaus2::util::ArgParser const & arg)
{
	std::cerr << "usage: " << arg.progname << " -d<disks> -b<blocks> [flags] <output_prefix> <input>\n";
	std::cerr << std::endl;
	std::cerr << "mandatory options:\n";
	std::cerr << " -d<disks> : number of disks per batch\n";
	std::cerr << " -b<blocks>: number of blocks per stripe element\n";

	std::cerr << "\n";
	std::cerr << "optional flags:\n";
	std::cerr << " -v<level>       : verbosity level (default: 1)\n";
	std::cerr << " -T<tmpprefix>   : temp prefix (default: " << libmaus2::util::ArgInfo::getDefaultTmpFileName(arg.progname) << ")\n";
	std::cerr << " --disksize<size>: size of disks (default: " << getDefaultDiskSize() << ")\n";
}

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser arg(argc,argv);

		if ( arg.argPresent("h") || arg.argPresent("help") )
		{
			usage(arg);
			return EXIT_SUCCESS;
		}
		else if ( arg.size() < 2 )
		{
			usage(arg);
			return EXIT_FAILURE;
		}
		else
		{
			return optiraid(arg);
		}
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
