/*
    optiraid
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <DiskHeader.hpp>
#include <BlockHeader.hpp>
#include <DiskConstants.hpp>
#include <getOutputName.hpp>
#include <getHeaderDigestName.hpp>
#include <libmaus2/digest/DigestInterface.hpp>
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/util/TempFileRemovalContainer.hpp>
#include <libmaus2/aio/OutputStreamFactoryContainer.hpp>
#include <libmaus2/aio/ConcatInputStream.hpp>
#include <libmaus2/digest/DigestFactory.hpp>
#include <libmaus2/util/DirectoryStructure.hpp>

void loadHeader(DiskHeader & DH, std::string const & inputfn, libmaus2::digest::DigestInterface & udigest)
{
	libmaus2::aio::InputStreamInstance ISI(inputfn);
	DH.deserialise(ISI,udigest);
	// std::cerr << "[V] primary header size " << ISI.tellg() << std::endl;
}

static std::string getDecodePartName(
	std::string const & prefix,
	uint64_t const batchid,
	uint64_t const stripeid,
	uint64_t const subid,
	uint64_t const blockid
)
{
	std::ostringstream ostr;

	ostr << prefix
		<< "_batch_" << std::setw(6) << std::setfill('0') << batchid << std::setw(0)
		<< "_stripe_" << std::setw(6) << std::setfill('0') << stripeid << std::setw(0)
		<< "_sub_" << std::setw(6) << std::setfill('0') << subid << std::setw(0)
		<< "_block_" << std::setw(6) << std::setfill('0') << blockid << std::setw(0);

	return ostr.str();
}

// batch[0,numbatches)
// stripe[0,disks)
// sub[0,disks)
// block[0,blocks+1)
struct BlockIdComputer
{
	uint64_t const numbatches;
	uint64_t const disks;
	uint64_t const blocks;

	BlockIdComputer(
		uint64_t const rnumbatches,
		uint64_t const rdisks,
		uint64_t const rblocks
	) : numbatches(rnumbatches), disks(rdisks), blocks(rblocks)
	{

	}

	uint64_t operator()(uint64_t const batchid, uint64_t const stripeid, uint64_t const subid, uint64_t const blockid) const
	{
		return
			batchid*(disks*disks*(blocks+1))
			+
			stripeid*(disks*(blocks+1))
			+
			subid*(blocks+1)
			+
			blockid;
	}

	uint64_t size() const
	{
		return numbatches*disks*disks*(blocks+1);
	}

	std::string idToFN(std::string const & prefix, uint64_t id) const
	{
		uint64_t const blockid = id % (blocks+1);
		id /= (blocks+1);
		uint64_t const subid = id % disks;
		id /= disks;
		uint64_t const stripeid = id % disks;
		id /= disks;
		uint64_t const batchid = id;

		return getDecodePartName(prefix,batchid,stripeid,subid,blockid);
	}
};

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser const arg(argc,argv);
		uint64_t const verbose = arg.uniqueArgPresent("v") ? arg.getUnsignedNumericArg<uint64_t>("v") : 1;

		std::string const tmpfilebase = arg.uniqueArgPresent("T") ? arg["T"] : libmaus2::util::ArgInfo::getDefaultTmpFileName(arg.progname);
		std::string const tmpfn = tmpfilebase + "_tmp";
		libmaus2::util::TempFileRemovalContainer::addTempFile(tmpfn);
		libmaus2::digest::DigestInterface::unique_ptr_type uheaderdigest(libmaus2::digest::DigestFactory::constructStatic(getHeaderDigestName()));

		std::string const output = arg[0];

		std::vector<std::string> Vin;

		for ( std::string line; std::getline(std::cin,line); )
			Vin.push_back(line);

		if ( ! Vin.size() )
		{
			std::cerr << "[E] no input file names given" << std::endl;
			return EXIT_FAILURE;
		}

		DiskHeader DH;
		loadHeader(DH,Vin.front(),*uheaderdigest);

		if ( verbose > 1 )
			std::cerr << "[V] " << DH << std::endl;

		libmaus2::digest::DigestInterface::unique_ptr_type udatadigest(libmaus2::digest::DigestFactory::constructStatic(DH.sdig));

		uint64_t const digestlength = udatadigest->vdigestlength();
		libmaus2::autoarray::AutoArray<uint8_t> Adigest(digestlength,false);

		uint64_t const diskmetasize = DH.getSize(*uheaderdigest);

		uint64_t const blockheadersize = BlockHeader::getSize(*udatadigest,*uheaderdigest);

		DiskConstants const DC(DH.disksize,DH.blocks,DH.disks,diskmetasize,digestlength,DH.datasize,blockheadersize);
		uint64_t const readblocksize = 1024*1024;
		libmaus2::autoarray::AutoArray<char> B(readblocksize,false);

		BlockIdComputer const BIC(DC.numbatches,DH.disks,DH.blocks);
		uint64_t const totalblocks = BIC.size();

		libmaus2::util::DirectoryStructure DS("mem:dstmp",8 /* mod */,totalblocks,output);
		DS.doGenerate();

		for ( uint64_t z = 0; z < Vin.size(); ++z )
		{
			std::string const fn = Vin[z];

			libmaus2::aio::InputStreamInstance backISI(fn);

			DiskHeader DHz;

			try
			{
				DHz.deserialise(backISI,*uheaderdigest);
			}
			catch(std::exception const & ex)
			{
				std::cerr << ex.what() << std::endl;
				continue;
			}

			// std::cerr << "backISI.tellg()=" << backISI.tellg() << " diskmetasize=" << diskmetasize << std::endl;

			if ( ! backISI || backISI.tellg() != static_cast<std::streamsize>(diskmetasize) )
			{
				std::cerr << "[E] position wrong after reading header " << backISI.tellg() << " != " << diskmetasize << std::endl;
				std::cerr << "[E] header is " << DHz << std::endl;
				continue;
			}

			uint64_t const batchid = DHz.batchid;
			uint64_t const diskid = DHz.diskid;

			if ( verbose > 1 )
				std::cerr << "[V] fn=" << fn << " batchid=" << batchid << " diskid=" << diskid << std::endl;

			uint64_t const expectedblocks = DH.disks * (DH.blocks+1);

			for ( uint64_t b = 0; b < expectedblocks; ++b )
			{
				uint64_t const p = diskmetasize + DC.rawblocksize * b;

				try
				{
					backISI.clear();
					backISI.seekg(p);

					BlockHeader BH;
					BH.deserialise(backISI,*udatadigest,*uheaderdigest);

					#if 0
					uint64_t const stripeid = libmaus2::util::NumberSerialisation::deserialiseNumber(backISI);
					uint64_t const blockid = libmaus2::util::NumberSerialisation::deserialiseNumber(backISI);
					backISI.read(reinterpret_cast<char *>(Adigest.begin()),digestlength);

					if ( ! backISI || backISI.gcount() != static_cast<std::streamsize>(digestlength) )
					{
						libmaus2::exception::LibMausException lme;
						lme.getStream() << "[E] unable to read digest for stripeid=" << stripeid << " blockid=" << blockid << std::endl;
						lme.finish();
						throw lme;
					}
					#endif

					uint64_t const stripeid = BH.stripeid;
					uint64_t const blockid = BH.blockid;

					uint64_t const sub = (stripeid + DHz.diskid) % (DHz.disks);

					std::string const sdigest_expected(udatadigest->vdigestToString(BH.Adigest.begin()));

					uint64_t toread = DC.blockpayloadsize;
					udatadigest->vinit();

					libmaus2::aio::OutputStreamInstance::unique_ptr_type pOSI(
						new libmaus2::aio::OutputStreamInstance(tmpfn)
					);

					while ( toread )
					{
						uint64_t const r = std::min(toread,readblocksize);

						backISI.read(B.begin(),r);

						if ( ! backISI || backISI.gcount() != static_cast<std::streamsize>(r) )
						{
							libmaus2::exception::LibMausException lme;
							lme.getStream() << "[E] unable to read data for stripeid=" << stripeid << " blockid=" << blockid << std::endl;
							lme.finish();
							throw lme;
						}

						pOSI->write(
							B.begin(),r
						);

						if ( ! *pOSI )
						{
							libmaus2::exception::LibMausException lme;
							lme.getStream() << "[E] unable to write data to tmp file for stripeid=" << stripeid << " blockid=" << blockid << std::endl;
							lme.finish();
							throw lme;
						}


						udatadigest->vupdate(reinterpret_cast<uint8_t const *>(B.begin()),r);

						toread -= r;
					}

					pOSI->flush();

					if ( ! *pOSI )
					{
						libmaus2::exception::LibMausException lme;
						lme.getStream() << "[E] unable to flush data to tmp file for stripeid=" << stripeid << " blockid=" << blockid << std::endl;
						lme.finish();
						throw lme;
					}

					pOSI.reset();

					std::string const sdigest_disk = udatadigest->vdigestAsString();

					if ( sdigest_disk != sdigest_expected )
					{
						libmaus2::exception::LibMausException lme;
						lme.getStream() << "[E] checksum mismatch for stripeid=" << stripeid << " blockid=" << blockid << std::endl;
						lme.finish();
						throw lme;
					}

					uint64_t const fileid = BIC(batchid,stripeid,sub,blockid);
					std::string const outdir = DS.getDirectoryFor(fileid);
					std::string const blockfn = outdir + "/" + getDecodePartName(output,batchid,stripeid,sub,blockid);

					libmaus2::aio::OutputStreamFactoryContainer::rename(tmpfn,blockfn);

					if ( verbose > 1 )
						std::cerr << "batchid=" << batchid << " stripeid=" << stripeid << " sub=" << sub << " blockid=" << blockid << " digest=" << sdigest_expected << " fn=" << blockfn << std::endl;
				}
				catch(std::exception const & ex)
				{
					std::cerr << "[E] " << ex.what() << std::endl;
				}
			}
		}

		for ( uint64_t batchid = 0; batchid < DC.numbatches; ++batchid )
		{
			for ( uint64_t stripeid = 0; stripeid < DC.disks; ++stripeid )
			{
				std::vector< std::vector<std::string> > VS;
				libmaus2::autoarray::AutoArray<libmaus2::aio::ConcatInputStream::unique_ptr_type> AISI(DC.disks);

				for ( uint64_t subid = 0; subid < DC.disks; ++subid )
				{
					std::vector<std::string> V;

					for ( uint64_t blockid = 0; blockid < DC.blocks+1; ++blockid )
					{
						uint64_t const fileid = BIC(batchid,stripeid,subid,blockid);
						std::string const outdir = DS.getDirectoryFor(fileid);
						std::string const blockfn = outdir + "/" + getDecodePartName(output,batchid,stripeid,subid,blockid);

						// V.push_back(getDecodePartName(output,batchid,stripeid,subid,blockid));
						V.push_back(blockfn);
					}

					VS.push_back(V);

					libmaus2::aio::ConcatInputStream::unique_ptr_type tptr(
						new libmaus2::aio::ConcatInputStream(V)
					);
					AISI[subid] = std::move(tptr);
				}

				uint64_t const stripesize = DC.blockpayloadsize * DH.blocks;
				uint64_t const numreadblocks = (stripesize + readblocksize - 1)/readblocksize;

				libmaus2::autoarray::AutoArray<uint8_t> B(readblocksize,false);
				libmaus2::autoarray::AutoArray<uint8_t> C(readblocksize,false);

				for ( uint64_t b = 0; b < numreadblocks; ++b )
				{
					uint64_t const low = b * readblocksize;
					uint64_t const high = std::min(low+readblocksize,stripesize);
					uint64_t const d = high-low;

					std::fill(C.begin(),C.end(),0);

					for ( uint64_t subid = 0; subid+1 < DC.disks; ++subid )
					{
						AISI[subid]->read(reinterpret_cast<char *>(B.begin()),d);
						assert ( *(AISI[subid]) && AISI[subid]->gcount() == static_cast<std::streamsize>(d) );

						for ( uint64_t i = 0; i < d; ++i )
							C[i] ^= B[i];
					}

					AISI[DC.disks-1]->read(reinterpret_cast<char *>(B.begin()),d);
					assert ( *(AISI[DC.disks-1]) && AISI[DC.disks-1]->gcount() == static_cast<std::streamsize>(d) );

					for ( uint64_t i = 0; i < d; ++i )
						assert ( C[i] == B[i] );

				}

				if ( verbose > 1 )
					std::cerr << "[V] checked parity for batchid=" << batchid << " stripeid=" << stripeid << " size=" << stripesize*(DC.disks-1) << std::endl;
			}
		}

		uint64_t todo = DH.datasize;
		udatadigest->vinit();
		for ( uint64_t batchid = 0; todo && batchid < DC.numbatches; ++batchid )
		{
			for ( uint64_t stripeid = 0; todo && stripeid < DH.disks; ++stripeid )
			{
				for ( uint64_t subid = 0; todo && (subid+1 < DH.disks); ++subid )
				{
					for ( uint64_t blockid = 0; blockid < DH.blocks; ++blockid )
					{
						uint64_t blockrest = DC.blockpayloadsize;

						uint64_t const fileid = BIC(batchid,stripeid,subid,blockid);
						std::string const outdir = DS.getDirectoryFor(fileid);
						std::string const blockfn = outdir + "/" + getDecodePartName(output,batchid,stripeid,subid,blockid);

						libmaus2::aio::InputStreamInstance ISI(blockfn);

						while ( todo && blockrest )
						{
							uint64_t const av = std::min(std::min(todo,blockrest),readblocksize);
							assert ( av );

							ISI.read(B.begin(),av);

							if ( (! ISI) || ISI.gcount() != static_cast<std::streamsize>(av) )
							{
								libmaus2::exception::LibMausException lme;
								lme.getStream() << "[E] unable to read block data for file " << blockfn << std::endl;
								lme.finish();
								throw lme;
							}

							std::cout.write(B.begin(),av);

							udatadigest->vupdate(reinterpret_cast<uint8_t const *>(B.begin()),av);

							todo -= av;
							blockrest -= av;
						}
					}
				}
			}
		}
		std::string const sdigest_disk = udatadigest->vdigestAsString();

		for ( uint64_t i = 0; i < BIC.size(); ++i )
		{
			std::string const fn = DS.getDirectoryFor(i) + "/" + BIC.idToFN(output,i);
			libmaus2::aio::FileRemoval::removeFile(fn);
		}
		DS.doRemove();

		if ( sdigest_disk == DH.inputdigest )
		{
			if ( verbose )
				std::cerr << "[V] digest ok: " << sdigest_disk << std::endl;
			return EXIT_SUCCESS;
		}
		else
		{
			std::cerr << "[E] digest mismatch: " << sdigest_disk << " != " << DH.inputdigest << std::endl;
			return EXIT_FAILURE;
		}
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
