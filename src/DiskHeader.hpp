/*
    optiraid
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#if ! defined(DISKHEADER_HPP)
#define DISKHEADER_HPP

#include <DiskHeaderElement.hpp>
#include <libmaus2/digest/DigestInterface.hpp>

struct DiskHeader
{
	uint64_t disksize;
	uint64_t disks;
	uint64_t blocks;
	uint64_t datasize;
	uint64_t batchid;
	uint64_t diskid;
	std::string sdig;
	std::string inputdigest;

	DiskHeader()
	{

	}
	DiskHeader(
		uint64_t const rdisksize,
		uint64_t const rdisks,
		uint64_t const rblocks,
		uint64_t const rdatasize,
		uint64_t const rbatchid,
		uint64_t const rdiskid,
		std::string const & rsdig,
		std::string const & rinputdigest
	) : disksize(rdisksize), disks(rdisks), blocks(rblocks), datasize(rdatasize), batchid(rbatchid), diskid(rdiskid), sdig(rsdig), inputdigest(rinputdigest)
	{

	}


	static std::string getMagic()
	{
		return "OPTIRAID";
	}

	static uint64_t getVersionNumber()
	{
		return 0;
	}

	static DiskHeaderElement const & getElement(std::map<std::string,DiskHeaderElement> const & M, std::string const & key)
	{
		std::map<std::string,DiskHeaderElement>::const_iterator const & it = M.find(key);

		if ( it == M.end() )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "DiskHeader::getElement: element for key " << key << " is not present" << std::endl;
			lme.finish();
			throw lme;
		}

		return it->second;
	}

	void deserialiseData(std::istream & in)
	{
		std::string const magic = getMagic();

		libmaus2::autoarray::AutoArray<char> Amagic(magic.size(),false);
		in.read(Amagic.begin(),magic.size());

		if ( !in || in.gcount() != static_cast<std::streamsize>(magic.size()) )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "DiskHeader::deserialise: failed to read magic" << std::endl;
			lme.finish();
			throw lme;
		}

		if ( std::string(Amagic.begin(),Amagic.begin()+magic.size()) != magic )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "DiskHeader::deserialise: magic is wrong" << std::endl;
			lme.finish();
			throw lme;
		}

		uint64_t const version = libmaus2::util::NumberSerialisation::deserialiseNumber(in);
		uint64_t const kvsize = libmaus2::util::NumberSerialisation::deserialiseNumber(in);

		if ( version != getVersionNumber() )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "DiskHeader::deserialise: version number is wrong" << std::endl;
			lme.finish();
			throw lme;
		}

		libmaus2::autoarray::AutoArray<char> AKV(kvsize,false);
		in.read(AKV.begin(),kvsize);

		if ( !in || in.gcount() != static_cast<std::streamsize>(kvsize) )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "DiskHeader::deserialise: failed to KV data" << std::endl;
			lme.finish();
			throw lme;
		}

		std::string const sKV(AKV.begin(),AKV.begin()+kvsize);
		std::istringstream istrkv(sKV);
		std::vector<DiskHeaderElement> VKV;
		std::map<std::string,DiskHeaderElement> MKV;

		while ( istrkv && istrkv.peek() != std::istream::traits_type::eof() )
		{
			DiskHeaderElement DHE;
			DHE.deserialise(istrkv);
			VKV.push_back(DHE);
			MKV[DHE.key] = DHE;
		}

		if ( ! VKV.size() )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "DiskHeader::deserialise: KV data is empty" << std::endl;
			lme.finish();
			throw lme;
		}
		if ( VKV.back().key != "term" )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "DiskHeader::deserialise: KV data is not terminated" << std::endl;
			lme.finish();
			throw lme;
		}

		disksize = getElement(MKV,"disksize").getValueAsU64();
		disks = getElement(MKV,"disks").getValueAsU64();
		blocks = getElement(MKV,"blocks").getValueAsU64();
		datasize = getElement(MKV,"datasize").getValueAsU64();
		batchid = getElement(MKV,"batchid").getValueAsU64();
		diskid = getElement(MKV,"diskid").getValueAsU64();
		sdig = getElement(MKV,"digest").value;
		inputdigest = getElement(MKV,"inputdigest").value;
	}

	void deserialise(std::istream & in, libmaus2::digest::DigestInterface & udigest)
	{
		deserialiseData(in);
		std::ostringstream dataostr;
		serialiseData(dataostr);
		std::string const data = dataostr.str();

		libmaus2::autoarray::AutoArray<uint8_t> Adigestdata(udigest.vdigestlength());
		udigest.vinit();
		udigest.vupdate(reinterpret_cast<uint8_t const *>(data.c_str()),data.size());
		udigest.digest(Adigestdata.begin());

		libmaus2::autoarray::AutoArray<uint8_t> Adigestdisk(udigest.vdigestlength());
		in.read(reinterpret_cast<char *>(Adigestdisk.begin()),Adigestdisk.size());
		if ( ! in || in.gcount() != static_cast<std::streamsize>(Adigestdisk.size()) )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "DiskHeader::deserialise: failed to read digest" << std::endl;
			lme.finish();
			throw lme;
		}

		if ( memcmp(Adigestdata.begin(),Adigestdisk.begin(),udigest.vdigestlength()) != 0 )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "DiskHeader::deserialise: digest mismatch" << std::endl;
			lme.finish();
			throw lme;
		}
	}

	void serialiseData(std::ostream & out) const
	{
		std::string const magic = getMagic();

		std::ostringstream headerkvstr;

		uint64_t kvsize = 0;

		kvsize += DiskHeaderElement("disksize",disksize).serialise(headerkvstr);
		kvsize += DiskHeaderElement("disks",disks).serialise(headerkvstr);
		kvsize += DiskHeaderElement("blocks",blocks).serialise(headerkvstr);
		kvsize += DiskHeaderElement("datasize",datasize).serialise(headerkvstr);
		kvsize += DiskHeaderElement("batchid",batchid).serialise(headerkvstr);
		kvsize += DiskHeaderElement("diskid",diskid).serialise(headerkvstr);
		kvsize += DiskHeaderElement("digest",sdig).serialise(headerkvstr);
		kvsize += DiskHeaderElement("inputdigest",inputdigest).serialise(headerkvstr);
		kvsize += DiskHeaderElement("term",0).serialise(headerkvstr);

		out.write(magic.c_str(),magic.size());
		if ( ! out )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "DiskHeader::serialise: failed to write magic" << std::endl;
			lme.finish();
			throw lme;
		}

		libmaus2::util::NumberSerialisation::serialiseNumber(out,getVersionNumber()); // version number
		libmaus2::util::NumberSerialisation::serialiseNumber(out,kvsize);

		std::string const headerkv = headerkvstr.str();
		assert ( headerkv.size() == kvsize );

		out.write(headerkv.c_str(),headerkv.size());
		if ( ! out )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "DiskHeader::serialise: failed to write key/value pairs" << std::endl;
			lme.finish();
			throw lme;
		}
	}

	void serialise(std::ostream & out, libmaus2::digest::DigestInterface & udigest) const
	{
		std::ostringstream dataostr;
		serialiseData(dataostr);
		std::string const data = dataostr.str();

		udigest.vinit();
		udigest.vupdate(
			reinterpret_cast<uint8_t const *>(data.c_str()),
			data.size()
		);
		libmaus2::autoarray::AutoArray<uint8_t> Adigest(udigest.vdigestlength());
		udigest.digest(Adigest.begin());

		out.write(data.c_str(),data.size());
		out.write(reinterpret_cast<char const *>(Adigest.begin()),udigest.vdigestlength());

		if ( ! out )
		{
			libmaus2::exception::LibMausException lme;
			lme.getStream() << "DiskHeader::serialise: failed to write" << std::endl;
			lme.finish();
			throw lme;
		}
	}

	uint64_t getSize(libmaus2::digest::DigestInterface & udigest) const
	{
		std::ostringstream ostr;
		serialise(ostr,udigest);
		return ostr.str().size();
	}
};

std::ostream & operator<<(std::ostream & out, DiskHeader const & DH);
#endif
